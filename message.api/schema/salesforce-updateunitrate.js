module.exports = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Update opportunity product data ",
  "description": "Update Opp Product once Product data is updated from salesforce",
  "properties": {
    "opportunity_line_item_id": {
      "type": "string"
    }
  },
  "required": [
    "opportunity_line_item_id"
  ],
  "additionalProperties": true,
  "example": {
    "opportunity_line_item_id": "hasgdGDGAGG5623KJHKH"
  }
}

