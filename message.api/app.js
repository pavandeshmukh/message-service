"use strict";
/* 
    AWS lambda method are normally under 
    /var/task/filename.js
    As long as our project is not in the same structure, we can safely assume local vs AWS lambda
*/
const IS_LAMBDA = __filename.indexOf("/var/task/") != -1
console.log("IS_LAMBDAsadas", __filename.indexOf("/var/task/"));
console.log("IS_LAMBDA", IS_LAMBDA);

// This is for schema test and won't write to the actual stream
const VALIDATE_ONLY = false;

const AWS = IS_LAMBDA ? require('aws-sdk') : require('./node_modules/aws-sdk')

AWS.config.update({
  region: "us-east-1"
})


// Stage Stream
const STREAM_NAME = "stage-message-service-stream";
// Prod Stream
//var STREAM_NAME = "message-service-stream";

// The JSON Schema Validation Library
const Ajv = require('ajv');

// Single instance
const ajv = new Ajv({ allErrors: true });

// SAMPLE init in node for local, not needed for AWS Lambda if IS_LAMBDA is set to true
var exports = exports || {};

const contextHandler = { error: function () { }, done: function () { }, succeed: function (obj) { console.log(obj) } };


exports.handler = function (event, context) {
  if (!event.producer || !event.payload) {
    context.succeed({ success: false, message: "Invalid contract, please check the documentation to ensure supported contract is submitted" })
    return;
  }
  let schema = null;
  let payload = event.payload;
  try {
    // schema lookup
    schema = require('./schema/' + event.producer);
  }
  catch (e) {
    context.succeed({ success: false, message: "Invalid producer, please check the documentation to ensure supported producer is submitted" })
    return;
  }

  // Schema validation
  let validate = ajv.compile(schema);
  if (!validate(payload)) {
    console.log(validate.errors)
    context.succeed({ success: false, message: `Errors occurred with producer ${event.producer} validation, see errors for more details`, errors: validate.errors })
  } else {
    if (VALIDATE_ONLY) {
      return context.succeed({ success: true, message: `Done validating ${event.producer}` })
    }
    let kinesis = new AWS.Kinesis({ apiVersion: '2013-12-02' });

    // Record
    let records = [
      {
        Data: JSON.stringify(payload), /* required */
        PartitionKey: event.producer, /* producer partition key*/
      }
    ]
    // Put to STREAM
    kinesis.putRecords(
      {
        Records: records,
        StreamName: STREAM_NAME /* required */
      },
      function (err, data) {
        let responseObj = { success: false }
        if (err) {
          console.log(err, err.stack);
          responseObj.message = `Fail to write message to stream due to: ${JSON.stringify(err)}`; // an error occurred 
        }
        else {
          console.log(`${STREAM_NAME}:${event.producer} was created successfully`); // successful response
          responseObj.success = true;
        }
        context.succeed(responseObj)
      }
    );

  }
}

// producer test
var testObjs = {

  // salesforce-updateunitrate
  "salesforce-updateunitrate": {
    "producer": "salesforce-updateunitrate",
    "payload": {
      "opportunity_line_item_id": "hasgdGDGAGG5623KJHKH"
    }
  },

  // composer-report-rollup, this normally trigger by program_account when it's completed'
  "negative-testing": {
    "producer": "negative-testing",
    "payload": {
      "client_id": 1000,
      "opportunity_line_item_id": "hasgdGDGAGG5623KJHKHfssdf"
    }
  }
}

if (!IS_LAMBDA) {
  for (let eventObj of objToMap(testObjs).values())
    exports.handler(eventObj, contextHandler)
}

// Method to convert a normal javascript object to Map
function objToMap(obj) {
  return Object.keys(obj).reduce((map, key) => map.set(key, obj[key]), new Map());
}