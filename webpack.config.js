const path = require("path");
const slWebpack = require("serverless-webpack");
const nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: slWebpack.lib.entries,
  target: "node",
  mode: slWebpack.lib.webpack.isLocal ? "development" : "production",
  optimization: {
    // We no not want to minimize our code.
    minimize: slWebpack.lib.options.stage === "production"
  },
  performance: {
    // Turn off size warnings for entry points
    hints: false
  },
  externals: [nodeExternals()],
  output: {
    libraryTarget: "commonjs2",
    path: path.join(__dirname, ".webpack"),
    filename: "[name].js"
  }
};
